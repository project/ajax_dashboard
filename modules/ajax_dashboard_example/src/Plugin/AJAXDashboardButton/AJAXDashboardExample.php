<?php

namespace Drupal\ajax_dashboard_example\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Class AJAXDashboardExample.
 *
 * @package Drupal\ajax_dashboard_example\Plugin\AJAXDashboardButton
 *
 * @AJAXDashboardButton (
 *   id = "ajax_dashboard_example",
 *   label = "AJAX Dashboard Example"
 * )
 */
class AJAXDashboardExample extends AJAXDashboardButtonBase {

  /**
   * {@inheritdoc}
   */
  public static function access(array $params = [], array $button_data = [], AccountInterface $account = NULL) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    sleep(1);
    return [
      [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => t('AJAX Dashboard Example'),
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => t('This is an example plugin which includes a one-second delay so that you can see load behaviors.'),
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => t('Look at ajax_dashboard/src/Plugin/AJAXDashboardButton/AJAXDashboardExample.php for an example of how to create an AJAX Dashboard plugin.'),
      ],
    ];
  }

}
