<?php

namespace Drupal\ajax_dashboard_example\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Session\AccountInterface;

/**
 * Class AJAXDashboardJSAlertExample.
 *
 * @package Drupal\ajax_dashboard_example\Plugin\AJAXDashboardButton
 *
 * @AJAXDashboardButton (
 *   id = "ajax_dashboard_js_alert",
 *   label = "AJAX Dashboard JS Alert"
 * )
 */
class AJAXDashboardJSAlertExample extends AJAXDashboardButtonBase {

  /**
   * {@inheritdoc}
   */
  public static function access(array $params = [], array $button_data = [], AccountInterface $account = NULL) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    return [
      [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => t('AJAX Dashboard JS Alert Example'),
      ],
      [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => t('This is an example plugin which includes a set of JS alerts.'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function AddAJAXResponseCommands(AjaxResponse $response = NULL, array $params = [], array $active_button = []) {
    $response->addCommand(new \Drupal\Core\Ajax\AlertCommand('This example loads a Javascript alert, and the button code illustrates how to add multiple AJAX commands to a button.'));
    $response->addCommand(new \Drupal\Core\Ajax\AlertCommand('This is a second alert command.'));
    return $response;
  }

}
