<?php

namespace Drupal\ajax_dashboard_views\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Views;

/**
 * Class View.
 *
 * @package Drupal\ajax_dashboard_example\Plugin\AJAXDashboardSection
 *
 * @AJAXDashboardButton (
 *   id = "view",
 *   label = "AJAX Dashboard View"
 * )
 */
class View extends AJAXDashboardButtonBase {

  /**
   * Extract the view and view display from the button data.
   *
   * @param array $button_data
   *   The button data containing the information.
   *
   * @return array
   *   An array containing the information needed to build the view.
   */
  public static function viewData(array $button_data = []) {
    return [
      'view' => $button_data['view'],
      'display' => isset($button_data['display']) ? $button_data['display'] : 'default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function access(array $params = [], array $button_data = [], AccountInterface $account = NULL) {
    $view_data = self::viewData($button_data);
    $view = Views::getView($view_data['view']);
    return $view->access($view_data['display']);
  }

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    $view_data = self::viewData($button_data);
    $view = Views::getView($view_data['view']);
    $view->setDisplay($view_data['display']);
    $args = [];
    if ($button_data['args']) {
      foreach ($button_data['args'] as $arg) {
        if (isset($params[$arg])) {
          $args[] = $params[$arg];
        }
      }
    }
    if (!empty($args)) {
      $view->setArguments($args);
    }
    return $view->preview();
  }

}
