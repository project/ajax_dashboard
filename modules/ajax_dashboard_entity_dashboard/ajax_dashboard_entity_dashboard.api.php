<?php

/**
 * @file
 * API documentation for AJAX Dashboard module.
 */


/**
 * Require certain entities to have AJAX Dashboards attached.
 *
 * This returns and array with an entity type key,
 * which in turn contains a keyed array bundle => bundle.
 *
 * Entities of the selected bundles will have an AJAX Dashboard
 * set up for them after visiting
 * /admin/config/system/ajax_dashboard/entities .
 *
 * Note that entities can also have dashboards added without the hook by
 * visiting /admin/config/system/ajax_dashboard/entities , but these can
 * be disabled. Entities/bundles identified by this hook cannot disable a
 * dashboard.
 *
 * @return array
 *   an array of entity types and bundles that will have an
 * AJAX Dashboard attached.
 *
 */
function hook_ajax_dashboard_entity_dashboard() {
  return [
    // Require a dashboard for nodes of my_type.
    'node' => ['my_type' => 'my_type'],
    // Require a user dashboard.
    'user' => ['user' => 'user']
  ];
}

