<?php

namespace Drupal\ajax_dashboard_entity_dashboard\Form;

use Drupal\ajax_dashboard\Entity\AJAXDashboard;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AJAXDashboardEntityDashboardForm.
 *
 * @package Drupal\ajax_dashboard_entity_dashboard\Form
 *
 * @ingroup ajax_dashboard
 */
class AJAXDashboardEntityDashboardForm extends FormBase {

  /**
   * Gets the bundle info of an entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * User.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * AutoEntityLabelController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   Bundle Information.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module Handler.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   Account Interface.
   */
  public function __construct(
    EntityTypeBundleInfoInterface $bundle_info,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $moduleHandler,
    AccountInterface $user
  ) {
    $this->bundleInfo = $bundle_info;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $moduleHandler;
    $this->user = $user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('current_user')
    );
  }


  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ajax_dashboard_entity_dashboard_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['ajax_dashboard_settings']['#markup'] = $this->t('This page will allow you to enable dashboards as a display field on content entities.');
    $form['ajax_dashboard_settings']['#markup'] .= $this->t('You may also force an entity to require this dashboard by using hook_ajax_dashboard_entity_dashboard.');

    $config = $this->configFactory->getEditable('ajax_dashboard_entity_dashboard.settings');
    $enabled_entities = $config->get('enabled_entities');
    $definitions = $this->entityTypeManager->getDefinitions();

    $required_entities = $this->moduleHandler->invokeAll('ajax_dashboard_entity_dashboard');

    $form['entities'] = ['#type' => 'container'];

    foreach ($definitions as $type => $definition) {
      if($definition->getGroup() === 'content') {
        $form['entities'][$type]['#type'] = 'html_tag';
        $form['entities'][$type]['#tag'] = 'strong';
        $form['entities'][$type]['#value'] = $definition->getLabel();
        $bundles = [];
        if ($definition->getBundleEntityType()) {
          $bundle_info = $this->bundleInfo->getBundleInfo($type);
          foreach($bundle_info as $bundle => $info) {
            $bundles[$bundle] = $info['label'];
          }
        }
        else {
          $bundles = [$type => $definition->getLabel()];
        }

        foreach ($bundles as $bundle => $label) {
          $form['entities'][$type . '|' . $bundle]['#type'] = 'checkbox';
          $form['entities'][$type . '|' . $bundle]['#title'] = $label;
          if (isset($enabled_entities[$type][$bundle])) {
            $form['entities'][$type . '|' . $bundle]['#default_value'] = TRUE;
          }
          if (isset($required_entities[$type][$bundle])) {
            $form['entities'][$type . '|' . $bundle]['#default_value'] = TRUE;
            $form['entities'][$type . '|' . $bundle]['#disabled'] = TRUE;
          }
        }

      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $definitions = $this->entityTypeManager->getDefinitions();
    $enabled_entities = [];
    foreach ($definitions as $type => $definition) {
      foreach ($values as $bundle => $value) {
        if ($value && strpos($bundle, $type) !== FALSE) {
          $bundle_explode = explode('|', $bundle);
          $bundle_id = $bundle_explode[1];
          $enabled_entities[$type][$bundle_id] = $bundle_id;
          $dashboard_config = $this->configFactory->getEditable('ajax_dashboard.ajax_dashboard.' . $type . '__' . $bundle_id);
          // Create a default dashboard.
          if (empty($dashboard_config->getRawData())) {
            $dashboard_config_data = [
              'id' => $type . '__' . $bundle_id,
              '#attributes' => ['data-placeholder' => 'Loading...'],
              'label' => 'Dashboard',
              '#attached' => ['library' => []],
              'dashboard' => ['#attributes' => ['class' => []]],
              'controls' => [
                'todo_controls' => [
                  'label' => 'To-do List',
                  'buttons' => [
                    'todo_configure_dashboard' => [
                      'plugin' => 'constant',
                      'label' => 'Configure this dashboard',
                      'constant' => 'This dashboard was created automatically by the ajax_dashboard_entity_dashboard module, and may be configured at /admin/structure/ajax_dashboard .'
                    ]
                  ],
                ],
              ],
            ];
            $set_data = [
              'id' => $type . '__' . $bundle_id,
              'status' => TRUE,
              'data' => Yaml::encode($dashboard_config_data),
            ];
            $dashboard = AJAXDashboard::create($set_data);
            $dashboard->save();
          }
        }
      }
    }
    $config = $this->configFactory->getEditable('ajax_dashboard_entity_dashboard.settings');
    $config->setData(['enabled_entities' => $enabled_entities])->save(TRUE);
    $this->messenger()->addMessage('The configuration options have been saved.');
  }

}
