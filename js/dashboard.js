/**
 * @file
 * AJAX Dashboard JS
 */

(function ($, Drupal) {

  'use strict';

  // source: https://stackoverflow.com/questions/5999118/how-can-i-add-or-update-a-query-string-parameter
  function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
    var separator = uri.indexOf('?') !== -1 ? '&' : '?';
    if (uri.match(re)) {
      return uri.replace(re, '$1' + key + '=' + value + '$2');
    }
    else {
      return uri + separator + key + '=' + value;
    }
  }

  Drupal.behaviors.ajaxDashboard = {
    attach: function (context, settings) {
      $('.ajax-dashboard-button', context).click(function () {
        var content_url = $(this).data('content-url');
        var ajax_content_url = Drupal.url.toAbsolute(content_url);
        var current_url = window.location.pathname;
        var button_id = $(this).data('button-id');

        var dashboard_display_id = 'ajax-dashboard-display--' + $(this).data('dashboard-id');
        var dashboard_placeholder = $('#ajax-dashboard--' + $(this).data('dashboard-id')).data('placeholder');
        $('#' + dashboard_display_id).text(dashboard_placeholder);

        var qstring = updateQueryStringParameter(window.location.search, 'dashboard', button_id);
        var u = window.location.pathname + qstring;
        history.pushState({}, '', u);
        // Now, set a cookie to remember the form someone looked at previously.
        document.cookie = 'ajax_dashboard__' + settings.ajax_dashboard.dashboard_id + '=' + button_id + ';path=/';
        document.cookie = 'ajax_dashboard_lastpath' + '=' + current_url + ';path=/';

        Drupal.ajax({url: ajax_content_url}).execute();
      });
    }
  };
})(jQuery, Drupal);
