<?php

namespace Drupal\ajax_dashboard\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AJAX Dashboard Control item annotation object.
 *
 * @see \Drupal\ajax_dashboard\Plugin\AJAXDashboardControlanager
 * @see plugin_api
 *
 * @Annotation
 */
class AJAXDashboardControl extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
