<?php

namespace Drupal\ajax_dashboard\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a AJAX Dashboard Button item annotation object.
 *
 * @see \Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonManager
 * @see plugin_api
 *
 * @Annotation
 */
class AJAXDashboardButton extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
