<?php

namespace Drupal\ajax_dashboard\Controller;

use Drupal\ajax_dashboard\AJAXDashboard;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class AJAXDashboardController.
 *
 * @package Drupal\ajax_dashboard\Controller
 */
class AJAXDashboardController extends ControllerBase {

  /**
   * Take in dashboard info from an AJAX request, and rebuild the dashboard.
   *
   * Dashboard info comes in through query parameters.
   *
   * @return AjaxResponse|RedirectResponse
   *   A responses containing the data needed to create the dashboard.
   */
  public function loadDashboard() {
    $request = \Drupal::request();
    $is_ajax_request = $request->get('_drupal_ajax');
    if ($is_ajax_request) {
      $query = $request->query;
      $params = ['params' => []];
      foreach ($query->all() as $key => $value) {
        if (strpos($key, 'param__') !== FALSE) {
          $k = str_replace('param__', '', $key);
          $params['params'][$k] = $value;
        }
        else {
          $params[$key] = $value;
        }
      }
      $active_button = AJAXDashboard::getActiveButton($params);
      $build = AJAXDashboard::displayDashboard($params, $active_button);
      $response = new AjaxResponse();
      // @todo maybe we can make this a default part of the button base?
      $dashboard_id = Html::escape($params['dashboard']);
      $replace = new ReplaceCommand('#ajax-dashboard-display--' . $dashboard_id, $build);
      // Allow the active button to modify the response.
      if ($active_button && isset($active_button['class'])) {
        $response = $active_button['class']::AddAJAXResponseCommands($response, $params, $active_button);
      }

      $response->addCommand($replace);
      return $response;
    }
    else {
      $lastpath = $request->cookies->get('ajax_dashboard_lastpath');
      $url = Url::fromUserInput('/');
      if ($lastpath) {
        $url = Url::fromUserInput($lastpath);
      }
      $response = RedirectResponse::create($url->toString());
      return $response;
    }
  }

}
