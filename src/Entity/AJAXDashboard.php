<?php

namespace Drupal\ajax_dashboard\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\ajax_dashboard\AJAXDashboardInterface;

/**
 * Defines the AJAX Dashboard entity.
 *
 * @ConfigEntityType(
 *   id = "ajax_dashboard",
 *   label = @Translation("AJAX Dashboard"),
 *   handlers = {
 *     "list_builder" = "Drupal\ajax_dashboard\Entity\Controller\AJAXDashboardListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ajax_dashboard\Form\AJAXDashboardForm",
 *       "edit" = "Drupal\ajax_dashboard\Form\AJAXDashboardForm",
 *       "delete" = "Drupal\ajax_dashboard\Form\AJAXDashboardDeleteForm",
 *     }
 *   },
 *   config_prefix = "ajax_dashboard",
 *   admin_permission = "administer ajax_dashboard",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "data" = "data",
 *   },
 *   links = {
 *     "collection" = "/admin/structure/ajax_dashboard/list",
 *     "edit-form" = "/admin/structure/ajax_dashboard/{ajax_dashboard}",
 *     "delete-form" = "/admin/structure/ajax_dashboard/{ajax_dashboard}/delete",
 *   }
 * )
 */
class AJAXDashboard extends ConfigEntityBase implements AJAXDashboardInterface {

  /**
   * The AJAX Dashboard ID.
   *
   * @var string
   */
  public $id;

  /**
   * The AJAX Dashboard label.
   *
   * @var string
   */
  public $label;

  /**
   * The AJAX Dashboard status.
   *
   * @var string
   */
  public $status;

}
