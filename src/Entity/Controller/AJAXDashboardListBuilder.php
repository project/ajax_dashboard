<?php

namespace Drupal\ajax_dashboard\Entity\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of AJAX Dashboard entities.
 */
class AJAXDashboardListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Dashboard');
    $header['status'] = $this->t('Active');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['status'] = $entity->status ? $this->t('Yes') : $this->t('No');

    return $row + parent::buildRow($entity);
  }

}
