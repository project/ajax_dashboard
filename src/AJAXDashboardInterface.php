<?php

namespace Drupal\ajax_dashboard;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a AJAXDashboard entity.
 *
 * @ingroup ajax_dashboard
 */
interface AJAXDashboardInterface extends ConfigEntityInterface {

}
