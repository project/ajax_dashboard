<?php

namespace Drupal\ajax_dashboard\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AJAXDashboardSettingsForm.
 *
 * @package Drupal\ajax_dashboard\Form
 *
 * @ingroup ajax_dashboard
 */
class AJAXDashboardSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ajax_dashboard_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['ajax_dashboard_settings']['#markup'] = $this->t('Settings form for AJAX Dashboard Entities.');
    return $form;
  }

}
