<?php

namespace Drupal\ajax_dashboard\Form;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AJAXDashboardForm.
 *
 * @ingroup ajax_dashboard
 *
 * @package Drupal\ajax_dashboard\Form
 *
 * Form controller for the ajax_dashboard entity edit forms.
 */
class AJAXDashboardForm extends EntityForm {

  /**
   * Constructs an AJAXDashboardForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Build Bundle Type List.
   *
   * @return array
   *   List of entity types.
   */
  public function createEntityList() {
    $ret = [];
    foreach (\Drupal::entityTypeManager()->getDefinitions() as $type => $info) {
      // Is this a content/front-facing entity?
      if ($info instanceof ContentEntityType) {
        $label = $info->getLabel();
        if ($label instanceof TranslatableMarkup) {
          $label = $label->render();
        }
        $ret[$type] = $label;
      }
    }
    return $ret;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\ajax_dashboard\Entity\AJAXDashboard */
    $form = parent::form($form, $form_state);
    $ent = $this->entity;

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $ent->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$ent->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#description' => $this->t('Uncheck to disable the dashboard.'),
      '#default_value' => isset($ent->status) ? $ent->status : TRUE
    ];

    $form['data'] = [
      '#title' => $this->t('Data'),
      '#description' => $this->t('Enter the Dashboard YAML here.'),
      '#type' => 'textarea',
      '#required' => TRUE,
      '#default_value' => isset($ent->data) ? $ent->data : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Redirect to term list after save.
    $form_state->setRedirect('entity.ajax_dashboard.collection');
    $entity = $this->entity;
    $entity->save();
  }

  /**
   * Check whether an AJAX Dashboard Entity configuration entity exists.
   *
   * @param string $id
   *   The machine name of the AJAX Dashboard Entity.
   *
   * @return bool
   *   Whether the entity exists.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('ajax_dashboard')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
