<?php

namespace Drupal\ajax_dashboard;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface AJAXDashboardManagerInterface.
 *
 * @package Drupal\ajax_dashboard
 */
interface AJAXDashboardManagerInterface extends PluginManagerInterface {

}
