<?php

namespace Drupal\ajax_dashboard\Plugin;

use Drupal\ajax_dashboard\AJAXDashboard;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Session\AccountInterface;

/**
 * Class AJAXDashboardControlBase.
 *
 * TODO Put control annotation into practice - we aren't using this yet.
 *
 * @package Drupal\ajax_dashboard\Plugin
 */
abstract class AJAXDashboardControlBase extends PluginBase implements AJAXDashboardControlInterface {

  /**
   * Determine access to control and content.
   *
   * @param array $params
   *   Associative array of parameters passed to control theme array.
   * @param array $control_data
   *   Associative array of control data provided from parent dashboard.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account for access checking. Set to NULL for current user.
   *
   * @return bool
   *   True if the user has access, false if not.
   */
  public static function access(array $params = [], array $control_data = [], AccountInterface $account = NULL) {
    $account = $account ? $account : \Drupal::currentUser();
    return TRUE;
  }


  /**
   * Get the list of buttons provided by this control, and appropriate data.
   *
   * Note that this is called when AJAX Dashboard is selecting an active button.
   * @see AJAXDashboard::getActiveButton()
   *
   * @param array $dashboard_params
   *   Associative array of parameters used to build a dashboard.
   * @param array $control
   *   Associative array of control data provided from parent dashboard.
   * @param array $dashboard
   *   Dashboard configuration
   * @return array
   *   The active button and its data.
   */
  public static function getButtonData(array $dashboard_params = [], array $control = [], array $dashboard = []) {
    return [];
  }

  /**
   * Build an AJAX Dashboard Control.
   *
   * Controls are collections of buttons, which may be treated as separate
   * blocks of controls for theming purposes.
   *
   * @param array $variables
   *   Theme variables to modify.
   */
  public static function buildControl(array &$variables) {
    $control_data = $variables['control']['#control_data'];
    $check_hash = function ($k) {
      return strpos($k, '#') !== FALSE;
    };

    $attributes = array_filter($control_data, $check_hash, ARRAY_FILTER_USE_KEY);
    foreach ($attributes as $key => $value) {
      if ($key === '#attributes') {
        foreach ($value as $attr_key => $attr) {
          $variables['attributes'][$attr_key] = $attr;
        }
      }
      else {
        $variables[$key] = $value;
      }
    }
    // Add default class.
    $variables['attributes']['class'][] = "ajax-dashboard-control";

    // Control IDs are fixed by the dashboard ID and control ID.
    $variables['attributes']['id'] = "ajax-dashboard-control--" . $variables['control']['#dashboard_id'] . '--' . $variables['control']['#control_id'];
    $variables['control']['label'] = isset($control_data['label']) ? $control_data['label'] : '';
  }

}
