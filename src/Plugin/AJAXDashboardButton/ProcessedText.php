<?php

namespace Drupal\ajax_dashboard\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;

/**
 * Class ProcessedText.
 *
 * Set formatted using the 'text' and 'format' keys in button data.
 * You may also pass a 'langcode' key to set the language.
 *
 * @package Drupal\ajax_dashboard\Plugin\AJAXDashboardButton
 *
 * @AJAXDashboardButton (
 *   id = "processed_text",
 *   label = "Processed Text"
 * )
 */
class ProcessedText extends AJAXDashboardButtonBase {

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    if (isset($button_data['text']) && !empty($button_data['format'])) {
      return [
        '#type' => "processed_text",
        '#text' => $button_data['text'],
        '#format' => $button_data['format'],
        '#langcode' => !empty($button_data['langcode']) ? $button_data['langcode']: \Drupal::currentUser()->getPreferredLangcode(),
      ];
    }
    return ['#markup' => ''];
  }

}
