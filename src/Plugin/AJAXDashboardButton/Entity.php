<?php

namespace Drupal\ajax_dashboard\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;

/**
 * Class Entity.
 *
 * @package Drupal\ajax_dashboard\Plugin\AJAXDashboardButton
 *
 * @AJAXDashboardButton (
 *   id = "entity",
 *   label = "Entity"
 * )
 */
class Entity extends AJAXDashboardButtonBase {

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    // Allow button data to override parameters, so that one can specify button data.
    $entity_type = !empty($params['entity_type']) ? $params['entity_type'] : '';
    $entity_type = !empty($button_data['entity_type']) ? $button_data['entity_type'] : $entity_type;
    $view_mode = !empty($params['view_mode']) ? $params['view_mode'] : '';
    $view_mode = !empty($button_data['view_mode']) ? $button_data['view_mode'] : $view_mode;
    $entity_id = !empty($params['entity_id']) ? $params['entity_id'] : '';
    $entity_id = !empty($button_data['entity_id']) ? $button_data['entity_id'] : $entity_id;
    // The entity_dashboard module uses 'entity' instead of 'entity_id'
    if (empty($entity_id) && (!empty($params['entity']) || !empty($button_data['entity']))) {
      $entity_id = !empty($params['entity']) ? $params['entity'] : $entity_id;
      $entity_id = !empty($button_data['entity']) ? $button_data['entity'] : $entity_id;
    }
    if (!empty($entity_type) && !empty($view_mode) && !empty($entity_id)) {
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
      $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
      return $view_builder->view($entity, $view_mode);
    }
    return ['#markup' => ''];
  }


}
