<?php

namespace Drupal\ajax_dashboard\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;

/**
 * Class RenderArray.
 *
 * Set a render array using the 'render_array' key in button data,
 * and it will be used to render the appropriate content when clicked.
 *
 * @package Drupal\ajax_dashboard\Plugin\AJAXDashboardButton
 *
 * @AJAXDashboardButton (
 *   id = "render_array",
 *   label = "Render Array"
 * )
 */
class RenderArray extends AJAXDashboardButtonBase {

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    if (isset($button_data['render_array']) && !empty($button_data['render_array'])) {
      return $button_data['render_array'];
    }
    return ['#markup' => ''];
  }

}
