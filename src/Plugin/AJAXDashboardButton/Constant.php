<?php

namespace Drupal\ajax_dashboard\Plugin\AJAXDashboardButton;

use Drupal\ajax_dashboard\Plugin\AJAXDashboardButtonBase;

/**
 * Class Constant.
 *
 * Set a constant markup using the 'constant' key in button data.
 *
 * @package Drupal\ajax_dashboard\Plugin\AJAXDashboardButton
 *
 * @AJAXDashboardButton (
 *   id = "constant",
 *   label = "Constant"
 * )
 */
class Constant extends AJAXDashboardButtonBase {

  /**
   * {@inheritdoc}
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    if (isset($button_data['constant'])) {
      return ['#markup' => t($button_data['constant'])];
    }
    return ['#markup' => ''];
  }

}
