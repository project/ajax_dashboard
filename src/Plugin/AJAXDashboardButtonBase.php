<?php

namespace Drupal\ajax_dashboard\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Session\AccountInterface;

/**
 * Class AJAXDashboardButtonBase.
 *
 * @package Drupal\ajax_dashboard\Plugin
 */
abstract class AJAXDashboardButtonBase extends PluginBase implements AJAXDashboardButtonInterface {

  /**
   * Determine access to button and content.
   *
   * @param array $params
   *   Associative array of parameters passed to button theme array.
   * @param array $button_data
   *   Associative array of button data provided from parent dashboard.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account for access checking. Set to NULL for current user.
   *
   * @return bool
   *   True if the user has access, false if not.
   */
  public static function access(array $params = [], array $button_data = [], AccountInterface $account = NULL) {
    $account = $account ? $account : \Drupal::currentUser();
    return TRUE;
  }

  /**
   * Get a button's status.
   *
   * The button status is a span with a status class and status text.
   * This function generates the status class.
   *
   * @param array $params
   *   Associative array of parameters passed to the parent dashboard.
   * @param array $button_data
   *   Associative array of button data, including the ID of the dashboard
   *   and of the parent control.
   *
   * @return string
   *   String which is placed into a span tag.
   */
  public static function getButtonStatus(array $params = [], array $button_data = []) {
    return 'neutral';
  }

  /**
   * Get the button's status text.
   *
   * The button status is a span with a status class and status text.
   * This function generates the status text.
   *
   * @param array $params
   *   Associative array of parameters passed to the parent dashboard.
   * @param array $button_data
   *   Associative array of button data, including the ID of the dashboard
   *   and of the parent control.
   *
   * @return mixed
   *   String or Translatable Markup which is placed into a span tag.
   */
  public static function getButtonStatusText(array $params = [], array $button_data = []) {
    return '';
  }

  /**
   * Generate the button label.
   *
   * @param array $params
   *   Associative array of parameters passed to the parent dashboard.
   * @param array $button_data
   *   Associative array of button data, including the ID of the dashboard
   *   and of the parent control.
   *
   * @return mixed
   *   String or Translatable Markup which is placed into a span tag.
   */
  public static function getButtonLabel(array $params = [], array $button_data = []) {
    if (isset($button_data['label'])) {
      return $button_data['label'];
    }
    else {
      return t('Button');
    }
  }

  /**
   * Get the content to display on the dashboard.
   *
   * @param array $params
   *   Associative array of parameters passed to the parent dashboard.
   * @param array $button_data
   *   Associative array of button data, including the ID of the dashboard
   *   and of the parent control.
   *
   * @return array
   *   Render array of dashboard content.
   */
  public static function getButtonDashboardContent(array $params = [], array $button_data = []) {
    return ['#markup' => t('Dashboard')];
  }

  /**
   * Modify the AJAX response with extra commands, if needed for this button.
   *
   * @param AjaxResponse $response
   *   AJAX Response to modify.
   * @param array $params
   *   Associative array of parameters passed to the parent dashboard.
   * @param array $active_button
   *   The selected button from the controller.
   *
   * @return AjaxResponse $response
   *   The modified response.
   */
  public static function AddAJAXResponseCommands(AjaxResponse $response = NULL, array $params = [], array $active_button = []) {
    return $response;
  }

}
