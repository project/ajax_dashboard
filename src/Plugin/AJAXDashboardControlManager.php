<?php

namespace Drupal\ajax_dashboard\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the AJAX Dashboard Control plugin manager.
 */
class AJAXDashboardControlManager extends DefaultPluginManager {

  /**
   * Constructs a new AJAXDashboardControlManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/AJAXDashboardControl', $namespaces, $module_handler, 'Drupal\ajax_dashboard\Plugin\AJAXDashboardControlInterface', 'Drupal\ajax_dashboard\Annotation\AJAXDashboardControl');

    $this->alterInfo('ajax_dashboard_ajax_dashboard_control_info');
    $this->setCacheBackend($cache_backend, 'ajax_dashboard_ajax_dashboard_control_plugins');
  }

}
