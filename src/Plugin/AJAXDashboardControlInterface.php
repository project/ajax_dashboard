<?php

namespace Drupal\ajax_dashboard\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for AJAX Dashboard Control plugins.
 */
interface AJAXDashboardControlInterface extends PluginInspectionInterface {

}
