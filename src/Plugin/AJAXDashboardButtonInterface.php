<?php

namespace Drupal\ajax_dashboard\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for AJAX Dashboard Button plugins.
 */
interface AJAXDashboardButtonInterface extends PluginInspectionInterface {

}
