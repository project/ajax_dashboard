<?php

namespace Drupal\ajax_dashboard\Plugin\AJAXDashboardControl;

use Drupal\ajax_dashboard\AJAXDashboard;
use Drupal\ajax_dashboard\Plugin\AJAXDashboardControlBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Class ButtonList.
 *
 * Generate a list of buttons based on configuration.
 *
 * @package Drupal\ajax_dashboard\Plugin\AJAXDashboardControl
 *
 * @AJAXDashboardControl (
 *   id = "button_list",
 *   label = "Button List"
 * )
 */
class ButtonList extends AJAXDashboardControlBase {

  /**
   * {@inheritdoc}
   */
  public static function access(array $params = [], array $control_data = [], AccountInterface $account = NULL) {
    return parent::access($params, $control_data, $account); // TODO: Change the autogenerated stub
  }

  /**
   * {@inheritdoc}
   */
  public static function getButtonData(array $dashboard_params = [], array $control = [], array $dashboard = []) {
    $sort_func = AJAXDashboard::class . '::sortByWeight';

    $button_manager = \Drupal::service('plugin.manager.ajax_dashboard_button');
    $button_definitions = $button_manager->getDefinitions();

    $buttons = [];
    if (isset($control['buttons'])) {
      uasort($control['buttons'], $sort_func);
      foreach ($control['buttons'] as $button_index => $button) {
        if (isset($button['plugin']) && isset($button_definitions[$button['plugin']])) {
          $button_plugin = $button_definitions[$button['plugin']];
          if ($button_plugin['class']::access($dashboard_params['params'], $button)) {
            $button_key_array = [
              $dashboard['id'],
              $control['id'],
              $button_index,
            ];
            $button_key = implode('_', $button_key_array);
            $buttons[$button_key] = $button_plugin;
            $buttons[$button_key]['button_data'] = $button;
            $buttons[$button_key]['button_data']['dashboard_id'] = $dashboard['id'];
            $buttons[$button_key]['button_data']['control_id'] = $control['id'];
            $buttons[$button_key]['button_data']['button_id'] = $button_index;
            $buttons[$button_key]['button_data']['params'] = $dashboard_params['params'];
          }
        }
      }
    }
    return $buttons;
  }

  public static function buildControl(array &$variables) {
    parent::buildControl($variables);
    $control_data = $variables['control']['#control_data'];

    // Add the buttons.
    $variables['control']['buttons'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
    ];

    foreach ($control_data['buttons'] as $button_key => $button) {
      if (strpos($button_key, '#') === 0) {
        $variables['control']['buttons'][$button_key] = $button;
      }
      else {
        $variables['control']['buttons'][$button_key] = [
          '#theme' => 'ajax_dashboard_button',
          '#dashboard_id' => $variables['control']['#dashboard_id'],
          '#control_id' => $variables['control']['#control_id'],
          '#button_id' => $button_key,
          '#button_data' => $button,
          '#params' => $variables['control']['#params'],
          '#weight' => isset($button['weight']) ? $button['weight'] : 0,
        ];
      }
    }
    $variables['control']['buttons']['#attributes']['class'][] = 'ajax-dashboard-buttons';
  }
}
