<?php

namespace Drupal\ajax_dashboard;

/**
 * Class AJAXDashboardControl.
 *
 * @package Drupal\ajax_dashboard
 */
class AJAXDashboardControl {

  /**
   * Build an AJAX Dashboard Control.
   *
   * Controls are collections of buttons, which may be treated as separate
   * blocks of controls for theming purposes.
   *
   * @param array $variables
   *   Theme variables to modify.
   */
  public static function buildControl(array &$variables) {
    $control_manager = \Drupal::service('plugin.manager.ajax_dashboard_control');
    $control_definitions = $control_manager->getDefinitions();

    $plugin = !empty($variables['control']['#control_data']['plugin']) ? $variables['control']['#control_data']['plugin'] : 'button_list';

    $control_definitions[$plugin]['class']::buildControl($variables);
  }

}
