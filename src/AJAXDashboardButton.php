<?php

namespace Drupal\ajax_dashboard;

use Drupal\Component\Utility\Html;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\Core\Url;

/**
 * Class AJAXDashboardButton.
 *
 * @package Drupal\ajax_dashboard
 */
class AJAXDashboardButton {

  /**
   * Build a dashboard button from the parameters and data.
   *
   * @param array $variables
   *   The theme variables being updated by this function.
   *
   * @throws MissingDataException
   *   If we have missing button data, this will be thrown.
   */
  public static function buildButton(array &$variables) {
    $button_data = $variables['button'];
    if (!isset($button_data['#button_data']['plugin'])) {
      throw new MissingDataException("A plugin was not specified for a dashboard button.");
    }
    $plugin_id = $button_data['#button_data']['plugin'];
    $params = isset($button_data['#params']) ? $button_data['#params'] : [];

    // First, determine whether we have a plugin, and whether we have access.
    $dashboard_button_manager = \Drupal::service('plugin.manager.ajax_dashboard_button');
    $dashboard_button_definitions = $dashboard_button_manager->getDefinitions();
    if (isset($dashboard_button_definitions[$plugin_id])) {
      $plugin_definition = $dashboard_button_definitions[$plugin_id];
      $plugin = $plugin_definition['class'];
      $variables['button']['access'] = $plugin::access($params, $button_data['#button_data']);
      $variables['button']['status'] = $plugin::getButtonStatus($params, $button_data['#button_data']);
      $variables['button']['status_text'] = $plugin::getButtonStatusText($params, $button_data['#button_data']);

      // Next,set the attributes and properties for the button.
      $check_hash = function ($k) {
        return strpos($k, '#') !== FALSE;
      };
      $attributes = array_filter($button_data['#button_data'], $check_hash, ARRAY_FILTER_USE_KEY);
      foreach ($attributes as $key => $value) {
        if ($key === '#attributes') {
          foreach ($value as $attr_key => $attr) {
            $variables['attributes'][$attr_key] = $attr;
          }
        }
        else {
          $variables[$key] = $value;
        }
      }

      $check_no_hash = function ($k) {
        return strpos($k, '#') === FALSE;
      };
      $button_vars = array_filter($button_data['#button_data'], $check_no_hash, ARRAY_FILTER_USE_KEY);
      foreach ($button_vars as $key => $value) {
        $variables['button'][$key] = $value;
      }
      // Make a custom label if the plugin provides one.
      // Otherwise, assume it was set earlier.
      $label = $plugin::getButtonLabel($params, $button_data['#button_data']);
      if ($label) {
        $variables['button']['label'] = $label;
      }
      // Add standard attributes for dashboard buttons.
      $id_array = [
        "ajax-dashboard-button",
        $variables['button']['#dashboard_id'],
        $variables['button']['#control_id'],
        $variables['button']['#button_id'],
      ];
      $variables['attributes']['id'] = implode('--', $id_array);
      $variables['attributes']['class'][] = "ajax-dashboard-button";
      // We might want to also provide status as a data attribute.
      $variables['attributes']['data-button-status'] = $plugin::getButtonStatus($params, $button_data['#button_data']);

      // Finally, set the query for fetching the content.
      $query = [
        'dashboard' => $variables['button']['#dashboard_id'],
        'control' => $variables['button']['#control_id'],
        'button' => $variables['button']['#button_id'],
      ];
      $button_id = implode('_', $query);
      foreach ($params as $param_key => $param) {
        if (is_string($param) || is_numeric($param)) {
          $param_key = Html::escape($param_key);
          $param = Html::escape($param);
          $query['param__' . $param_key] = $param;
        }
      }
      $content_url = Url::fromRoute('ajax_dashboard.load_dashboard', [], ['query' => $query]);
      $variables['attributes']['data-content-url'] = $content_url->toString();
      $variables['attributes']['data-dashboard-id'] = $variables['button']['#dashboard_id'];
      $variables['attributes']['data-button-id'] = $button_id;
    }
    else {
      throw new MissingDataException("A specified plugin, " . $plugin_id . ", was not supplied for a dashboard button.");
    }
  }

}
