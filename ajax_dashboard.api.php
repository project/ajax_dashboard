<?php

/**
 * @file
 * API documentation for AJAX Dashboard module.
 */


/**
 * Provide pre-processing of dashboard configuration.
 *
 * This will be called when the dashboard is first created, and will be saved.
 * This does not accept parameters.
 *
 * @param array $configuration
 *   Dashboard configuration.
 *
 * @see \Drupal\ajax_dashboard\\Drupal\ajax_dashboard\AJAXDashboard::getDashboardConfig()
 */
function hook_ajax_dashboard_alter(&$configuration) {
  // Add an intro button to my dashboard by default.
  if ($configuration['id'] === 'my_dashboard') {
    $dashboard_config['data']['controls']['introduction'] = [
      'id' => 'introduction',
      'plugin' => 'button_list',
      'label' => t('Introduction'),
      'weight' => -100, // Float intro to the top.
      'buttons' => [
        'intro' => [
          'id' => 'intro',
          'label' => t('Welcome!'),
          'plugin' => 'constant',
          'constant' => t('Welcome to this dashboard!'),
        ]
      ]
    ];
  }
}


/**
 * Provide post-processing of dashboard configuration.
 *
 * Modifies stored configuration when loaded, and result is not saved.
 * Accepts dashboard parameters.
 *
 * @param array $dashboard_config
 *   Dashboard configuration in array format.
 * @param array $params
 *   Dashboard parameters passed to the configuration.
 *
 * @see \Drupal\ajax_dashboard\\Drupal\ajax_dashboard\AJAXDashboard::getDashboardConfig()
 */
function hook_ajax_dashboard_config_alter(&$dashboard_config, &$params) {
  // Add an intro button to my dashboard if its a first time user.
  if ($dashboard_config['id'] === 'my_dashboard' && $params['first_time_user'] === TRUE) {
    $dashboard_config['data']['controls']['introduction'] = [
      'id' => 'introduction',
      'plugin' => 'button_list',
      'label' => t('Introduction'),
      'weight' => -100, // Float intro to the top.
      'buttons' => [
        'intro' => [
          'id' => 'intro',
          'label' => t('Welcome!'),
          'plugin' => 'constant',
          'constant' => t('Welcome to this dashboard!'),
        ]
      ]
    ];
  }
}
