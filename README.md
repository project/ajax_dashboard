# INTRODUCTION
AJAX Dashboard is a simple dashboard construction module, which allows 
developers to build AJAX-powered dashboards using a YAML configuration and a
selection of plugins.

# REQUIREMENTS
AJAX Dashbaord does not have any requirements. The included 
ajax_dashboard_views module depends on Views, which is a part of Drupal core.

# INSTALLATION
Install the module like any other Drupal module:
* Dev, via Composer: `composer require drupal/ajax_dashboard:2.x-dev`
* Latest release, via Composer: `composer require drupal/ajax_dashboard:^2.0`
* Manual: Download and extract the module to the modules folder in your 
  Drupal site diractory 

The module, by itself, does not provide any dashboards, and dashboards must be
defined in a module before they can be created. To create a dashboard, go to a
module's directory and create a [module_name].ajax_dashboard.yml file. See 
Configuration, below, for an example.

To load a dashboard, it must be provided as a render array:

```
$dashboard = [
  '#theme' => 'ajax_dashboard', 
  '#id' => 'the_id_of_your_dashboard', 
  '#params' => [
    'param1' => 'param1',
    'param2' => 'param2'
  ], 
];
```
The #params property will provide an array of parameters to each part of the
dashboard, which may be used programmatically, particularly for buttons.


# CONFIGURATION
A dashboard is typically defined in a [module_name].ajax_dashboard.yml like
this:

```
# Note: in this documentation we added the '#attributes' property
# everywhere that it will be recognized.
#
# The id property is used by the module, but the parent must be unique.
# Therefore, we normally just list the ID twice
dashboard_id:
  id: dashboard_id
  label: 'Dashboard Label'
  
  # This alows you to add attributes to the dashboard display.
  dashboard:
    '#attributes':
      class:
        - custom-display-class
   
  # You may pass render array attributes as #attributes
  '#attributes':
    class:
      - custom-class
    # data-placeholder provides a custom placeholder.
    # If not set, defaults to "Loading..."
    data-placeholder: |
      <p>Custom Placeholder, when loading a dashboard takes time.</p>
  
  # Controls are sections containing buttons.
  # These may be labeled and given attributes.
  controls:
    # These attributes will apply to the controls section
    '#attributes':
      class:
        - custom-control-class
    my_control:
      label: Control Section Label
      
      # Buttons will load the selected button plugin to the dashboard.
      buttons:
        my_button:
          '#attributes':
            class:
              - custom-button-class
          plugin: my_plugin
          # Plugin parameters are listed on the same level as 'plugin.'
          # See "Button Plugin API" for examples.
          param1: param1
          param2: param2

```

# THEMING
AJAX Dashboards are themable, and the dashboard overall, the sections, and
the buttons may be modified with custom templates. Use the templates in the
templates folder to get started with building your own templates.

## Template names
Name suggestions are listed as least specific to most specific, with less
specific templates overridden by more specific ones. 

### Dashboard
* ajax-dashboard
* ajax-dashboard--[dashboard_id]

### Control
* ajax-dashboard-control
* ajax-dashboard-control--[control_id]
* ajax-dashboard-control--[dashboard_id]
* ajax-dashboard-control--[dashboard_id]--[control_id]

### Button
* ajax-dashboard-button
* ajax-dashboard-button--[button_id]
* ajax-dashboard-button--[control_id]
* ajax-dashboard-button--[dashboard_id]
* ajax-dashboard-button--[control_id]--[button_id]
* ajax-dashboard-button--[dashboard_id]--[button_id]
* ajax-dashboard-button--[dashboard_id]--[control_id]
* ajax-dashboard-button--[dashboard_id]--[control_id]--[button_id]

# BUTTON PLUGIN API
AJAX Dashboard provides an @AJAXDashboardButton plugin to create and use custom
buttons for a dashboard. The code for creating a dashboard is stored in each
button plugin, and src/Plugin/AJAXDashboardButtonBase contains the base
functions and an explanation of each.

## Provided Plugins

### Constant
Returns whatever is provided in the configuration, such as a welcome message.

This does not check access. If you need it, set up a route, use the Route 
Plugin, and configure access on the route. Or, create a custom plugin.
```
Configuration parameters:
constant: A string to be shown in the dashboard.
```

### Views
Note: requires included AJAX Dashboard Views module

Load a View and display it in the dashboard. 

Includes Views access checking.
```
Configuration parameters:
view: The machine name of the view.
display: The machine name of the display. If not set, will default to 'default'
args: List of arguments. These are keys from the parameter array given to the
dashboard, and if the parameter isn't set, the argument will not be set. 
```

## Plugin functions

# INCLUDED MODULES

## AJAX Dashboard Views
This module provides an @AJAXDashboardButton plugin that lets you integrate 
with Views. See Button Plugin API > Provided Plugins > Views above for more
info.

## AJAX Dashboard Example
Working examples of an ajax_dashboard.yml file, custom @AJAXDashboardButton
plugin, custom dashboard and control templates (and theme registry in the 
.module file), and implementation as a user dashboard are all given in the
ajax_dashboard_example module.  

# CONTRIBUTORS
* laboratory.mike: https://www.drupal.org/u/laboratorymike
* Shawn2516: https://www.drupal.org/u/shawn2516
